//
//  CoachTableViewCell.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/6/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import SDWebImage

class CoachTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var bioLabel: UILabel?
    @IBOutlet var avatarImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        resize()
    }
    
    //MARK: - Configure
    
    func configure() {
        configureNameLabel()
        configureBioLabel()
    }
    
    func configureNameLabel() {
        self.nameLabel?.textColor = UIColor.defaultBlueColor()
        self.nameLabel?.font = UIFont.customAvenirNextMediumFontWithSize(22)
    }
    
    func configureBioLabel() {
        self.bioLabel?.textColor = UIColor.defaultSubtitleGrayTextColor()
        self.bioLabel?.font = UIFont.customAvenirNextRegularFontWithSize(16)
    }
    
    // MARK: - Resize
    
    func resize() {
        resizeNameLabel()
        resizeBioLabel()
    }
    
    func resizeNameLabel() {
        var nameLabelFrame: CGRect = (self.nameLabel?.frame)!
        self.nameLabel?.sizeToFit()
        nameLabelFrame.size.height = (self.nameLabel?.frame.size.height)!
        self.nameLabel?.frame = nameLabelFrame
    }
    
    func resizeBioLabel() {
        var bioLabelFrame: CGRect = (self.bioLabel?.frame)!
        self.bioLabel?.sizeToFit()
        bioLabelFrame.size.height = (self.bioLabel?.frame.size.height)!
        
        var nameLabelFrame: CGRect = (self.nameLabel?.frame)!
        nameLabelFrame.origin.y = (self.avatarImageView?.frame.origin.y)! + (((self.avatarImageView?.frame.size.height)! - (nameLabelFrame.size.height + bioLabelFrame.size.height)) / 2)
        self.nameLabel?.frame = nameLabelFrame
        
        
        bioLabelFrame.origin.y = nameLabelFrame.origin.y + nameLabelFrame.size.height
        self.bioLabel?.frame = bioLabelFrame
    }
    
    //MARK: - setCoach
    
    func setCoach(coachDictionary: [String: AnyObject]) {
        self.nameLabel?.text = nil
        self.bioLabel?.text = nil
        self.avatarImageView?.image = nil
        
        if let name: String = coachDictionary["AvatarName"] as? String {
            self.nameLabel?.text = name
        }
        
        if let bio: String = coachDictionary["AvatarBio"] as? String {
            self.bioLabel?.text = bio
        }
        
        if let avatarUrl: String = coachDictionary["AvatarPicturePath"] as? String {
            if let url: NSURL = NSURL.init(string: avatarUrl) {
                self.avatarImageView?.sd_setImageWithURL(url, placeholderImage: nil, completed: { (image, error, imageCacheType, url) in
                    if image != nil {
                        self.avatarImageView?.image = image
                    }
                    else {
                        self.avatarImageView?.image = nil
                    }
                });
            }
            
        }
        
        resize()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        if animated {
            UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseInOut, animations: { 
                self.setCellSelected(selected)
                }, completion:nil)
        }
        else {
            self.setCellSelected(selected)
        }
    }
    
    func setCellSelected(selected: Bool) {
        if selected {
            self.avatarImageView?.transform = CGAffineTransformMakeScale(1, 1)
            self.tintColor = UIColor.defaultBlueColor()
        }
        else {
            self.avatarImageView?.transform = CGAffineTransformMakeScale(0.8, 0.8)
            self.tintColor = UIColor.whiteColor()
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        setSelected(highlighted, animated: animated)
    }

}
