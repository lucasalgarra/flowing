//
//  CoachViewController.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/6/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

class CoachViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView?
    
    var refreshControl: UIRefreshControl = UIRefreshControl()
    var coachesArray: [[String: AnyObject]] = []
    var didUpdate = false

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.didUpdate {
            self.refreshControl.beginRefreshing()
            update()
            self.didUpdate = true
        }
    }
    
    // MARK: - Configure
    
    func configure() {
        configureNavigationBar()
        configureTableView()
    }
    
    func configureNavigationBar () {
        self.title = "Treinadores"
        
        let image: UIImage = UIImage.imageWithColor(UIColor.defaultNavigationBarColor(), size: CGSizeZero)
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.translucent = false
        self.setNeedsStatusBarAppearanceUpdate()
        
        let doneBarButtonItem: UIBarButtonItem = UIBarButtonItem.init(title: "Pronto", style: .Plain, target: self, action: #selector(self.done))
        self.navigationItem.rightBarButtonItem = doneBarButtonItem
    }
    
    func configureTableView() {
        self.tableView?.backgroundColor = self.view.backgroundColor;
        self.tableView?.separatorStyle = .None;
        
        
        self.refreshControl.tintColor = UIColor.defaultBlueColor()
        self.refreshControl.addTarget(self, action: #selector(self.update), forControlEvents: .ValueChanged)
        
        self.tableView?.addSubview(self.refreshControl)
        self.refreshControl.superview?.sendSubviewToBack(self.refreshControl)
    }

    // MARK: - Update
    
    func update() {
        myCoaches({ (coachesArray) in
            self.refreshControl.endRefreshing()
            
            if let itemsArray = coachesArray {
                self.coachesArray = itemsArray
                self.tableView?.reloadData()
            }
            
            }, failure: { (error) in
                self.refreshControl.endRefreshing()
        })
    }
    
    // MARK: - Actions
    
    func done() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coachesArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.size.height / 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let coachDictionary: [String: AnyObject] = self.coachesArray[indexPath.row]
        
        let cell: CoachTableViewCell = (tableView.dequeueReusableCellWithIdentifier("CoachTableViewCell") as? CoachTableViewCell)!
        cell.setCoach(coachDictionary)
        
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if let selected: NSNumber = coachDictionary["IsSelected"] as? NSNumber {
            if selected.boolValue {
                tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None);
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: - Storyboard
    
    
}
