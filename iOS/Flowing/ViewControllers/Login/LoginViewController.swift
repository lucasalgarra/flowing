//
//  LoginViewController.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/6/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

let kBackgrountDuration: NSTimeInterval = 5
let kButtonAlpha: CGFloat = 0.7

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var boxView: UIView!
    @IBOutlet var textFieldsBoxView: UIView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    @IBOutlet var loggingActivityIndicatorView: UIActivityIndicatorView!
    var backgroundTimer: NSTimer?
    var backgroundIndex: UInt32 = 0
    
    var textFieldsArray: [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configure()
        
//        self.emailTextField.text = "eduardo.guilarducci@flowing.com.br"
//        self.passwordTextField.text = "123456"
//        self.loginButton.enabled = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.addKeyboardObserver()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.passwordTextField.text = ""
        
        self.removeKeyboardObserver()
    }
    
    // MARK: - Configure
    
    func configure() {
        configureNavigationBar()
        configureBackgroundImageView()
        configureContentView()
        configureBoxView()
        configureTimer()
    }
    
    func configureNavigationBar () {
        self.title = ""
        let image: UIImage = UIImage.imageWithColor(UIColor.clearColor(), size: CGSizeZero)
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.translucent = true
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.loggingActivityIndicatorView.hidesWhenStopped = true
    }
    
    func configureBackgroundImageView() {
        self.backgroundImageView.backgroundColor = UIColor.blackColor()
        self.backgroundImageView.image = UIImage.init(named: "LoginBackground3")
    }
    
    func configureContentView() {
        let dismissKeyboardTapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.dismissKeyboard))
        self.contentView.addGestureRecognizer(dismissKeyboardTapGestureRecognizer)
    }
    
    func configureBoxView() {
        configureTextFieldsBoxView()
        configureLoginButton()
    }
    
    func configureTextFieldsBoxView() {
        let scale: CGFloat = UIScreen.mainScreen().scale
        
        self.textFieldsBoxView?.backgroundColor = UIColor.whiteColor()
        self.textFieldsBoxView?.layer.cornerRadius = 4
        self.textFieldsBoxView?.layer.masksToBounds = true
        self.textFieldsBoxView?.layer.borderWidth = 1/scale;
        self.textFieldsBoxView?.layer.borderColor = UIColor.defaultLineColor().CGColor
        configureTextFields()
    }
    
    func configureTextFields() {
        self.textFieldsArray = [self.emailTextField, self.passwordTextField]
        
        for textField: UITextField in self.textFieldsArray {
            textField.delegate = self
            textField.font = UIFont.customAvenirLightFontWithSize(16)
            textField.textColor = UIColor.defaultTitleGrayTextColor()
            textField.layer.masksToBounds = true
            textField.layer.borderWidth = (self.textFieldsBoxView?.layer.borderWidth)!
            textField.layer.borderColor = self.textFieldsBoxView?.layer.borderColor
        }
        
        let emailIconImageView: UIImageView = UIImageView.init(image: UIImage.init(named: "EmailIcon"))
        self.emailTextField.leftView = emailIconImageView
        self.emailTextField.leftViewMode = .Always
        
        let passwordIconImageView: UIImageView = UIImageView.init(image: UIImage.init(named: "PasswordIcon"))
        self.passwordTextField.leftView = passwordIconImageView
        self.passwordTextField.leftViewMode = .Always
    }
    
    func configureLoginButton() {
        self.loginButton.backgroundColor = UIColor.init(white: 0, alpha: kButtonAlpha)
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColor(), size: self.loginButton.frame.size), forState: .Normal)
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColorWithAlpha(kButtonAlpha), size: self.loginButton.frame.size), forState: .Disabled)
        self.loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.loginButton.layer.cornerRadius = (self.textFieldsBoxView?.layer.cornerRadius)!
        self.loginButton.layer.masksToBounds = true
        self.loginButton.enabled = false
    }
    
    func configureTimer() {
        if self.backgroundTimer != nil {
            self.backgroundTimer?.invalidate()
        }
        
        self.backgroundTimer = NSTimer.scheduledTimerWithTimeInterval(kBackgrountDuration, target: self, selector: #selector(self.changeBackgroundAnimated), userInfo: nil, repeats: true)
    }
    
    //MARK - Backgrounds
    
    func changeBackgroundAnimated() {
        let fakeImageView = UIImageView.init(frame: self.backgroundImageView.frame)
        fakeImageView.autoresizingMask = self.backgroundImageView.autoresizingMask
        fakeImageView.contentMode = self.backgroundImageView.contentMode
        fakeImageView.clipsToBounds = self.backgroundImageView.clipsToBounds
        fakeImageView.image = self.backgroundImageView.image
        self.backgroundImageView.superview?.addSubview(fakeImageView)
        self.backgroundImageView.superview?.sendSubviewToBack(fakeImageView)
        self.backgroundImageView.superview?.sendSubviewToBack(self.backgroundImageView)
        
        changeBackground()
        
        UIView.animateWithDuration(1, delay: 0, options: .CurveLinear, animations: {
            fakeImageView.alpha = 0
            }, completion: { finished in
                fakeImageView.image = nil
                fakeImageView.removeFromSuperview()
        })
    }
    
    func changeBackground() {
        self.backgroundIndex += 1
        
        if self.backgroundIndex >= 5 {
            self.backgroundIndex = 0
        }
        
        let imageName = String.init(format: "LoginBackground%@", NSNumber.init(unsignedInt: self.backgroundIndex))
        self.backgroundImageView.image = UIImage.init(named: imageName)
    }
    
    //MARK - Actions

    @IBAction func login(sender: UIButton?) {
        if let email: String = self.emailTextField.text {
            if email.characters.count > 0 {
                if let password = self.passwordTextField.text {
                    if password.characters.count > 0 {
                        didBeginLogging()
                        
                        loginWithEmail(email, password: password, success: { (userDictionary) in

                            didLogin()
                            
                            self.didEndLogging()

                            }, failure: { (loginError) in
                                self.didEndLogging()
                                
                                switch loginError {
                                case .LoginErrorInvalidCredentials:
                                    let alert = UIAlertController(title: "Credencial inválida", message: "Verifique seu email e senha e tente novamente", preferredStyle: UIAlertControllerStyle.Alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                    self.presentViewController(alert, animated: true, completion: nil)
                                default:
                                    let alert = UIAlertController(title: "Ops! Alguma coisa não deu certo", message: "Verifique sua conexão e tente novamente", preferredStyle: UIAlertControllerStyle.Alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                        })
                    }
                    else {
                        self.passwordTextField.becomeFirstResponder()
                    }
                }
                else {
                    self.passwordTextField.becomeFirstResponder()
                }
            }
            else {
                self.emailTextField.becomeFirstResponder()
            }
        }
        else {
            self.emailTextField.becomeFirstResponder()
        }
    }
    
    func didBeginLogging() {
        self.emailTextField.enabled = false
        self.passwordTextField.enabled = false
        self.loginButton.enabled = false
        self.loggingActivityIndicatorView.startAnimating()
    }
    
    func didEndLogging() {
        self.emailTextField.enabled = true
        self.passwordTextField.enabled = true
        self.loginButton.enabled = self.emailTextField.text?.characters.count > 0 && self.passwordTextField.text?.characters.count > 0
        self.loggingActivityIndicatorView.stopAnimating()
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField == self.emailTextField  {
            if self.passwordTextField.text?.characters.count == 0 {
                textField.returnKeyType = .Next
            }
            else {
                textField.returnKeyType = .Send
            }
        }
        
        if textField == self.passwordTextField  {
            if self.emailTextField.text?.characters.count == 0 {
                textField.returnKeyType = .Next
            }
            else {
                textField.returnKeyType = .Send
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.emailTextField  {
            if self.passwordTextField.text?.characters.count == 0 {
                self.passwordTextField.becomeFirstResponder()
            }
            else {
                login(nil)
            }
        }
        
        if textField == self.passwordTextField  {
            if self.emailTextField.text?.characters.count == 0 {
                self.emailTextField.becomeFirstResponder()
            }
            else {
                login(nil)
            }
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var newString: NSString = textField.text! as NSString
        newString = newString.stringByReplacingCharactersInRange(range, withString: string)
        
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColor(), size: self.loginButton.frame.size), forState: .Normal)
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColorWithAlpha(kButtonAlpha), size: self.loginButton.frame.size), forState: .Disabled)
        
        if textField == self.passwordTextField {
            if newString.length >= 6 && self.emailTextField.text?.characters.count > 0 {
                self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultBlueColor(), size: self.loginButton.frame.size), forState: .Normal)
                self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultBlueColorWithAlpha(kButtonAlpha), size: self.loginButton.frame.size), forState: .Disabled)
            }
            
            self.loginButton.enabled = newString.length > 0 && self.emailTextField.text?.characters.count > 0
        }
        
        if textField == self.emailTextField {
            if newString.length > 0 && self.passwordTextField.text?.characters.count >= 6  {
                self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultBlueColor(), size: self.loginButton.frame.size), forState: .Normal)
                self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultBlueColorWithAlpha(kButtonAlpha), size: self.loginButton.frame.size), forState: .Disabled)
            }
            
            self.loginButton.enabled = newString.length > 0 && self.passwordTextField.text?.characters.count > 0
        }
        
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColor(), size: self.loginButton.frame.size), forState: .Normal)
        self.loginButton.setBackgroundImage(UIImage.imageWithColor(UIColor.defaultGrayColorWithAlpha(kButtonAlpha), size: self.loginButton.frame.size), forState: .Disabled)
        self.loginButton.enabled = false
        return true
    }
    
    //MARK: - Keyboard
    
    func dismissKeyboard() {
        for textField: UITextField in self.textFieldsArray {
            textField.resignFirstResponder()
        }
    }
    
    func addKeyboardObserver() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillShowNotification), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillHideNotification), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardObserver() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShowNotification(aNotification: NSNotification) {
        if var info: [NSObject : AnyObject] = aNotification.userInfo {
            
            let animationDuration: NSNumber = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
            
            let animationCurve: NSNumber = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
            let options = UIViewAnimationOptions(rawValue: UInt(animationCurve.unsignedLongValue << 16))
            
            var keyboardFrame: CGRect = info[UIKeyboardFrameEndUserInfoKey]!.CGRectValue()
            keyboardFrame = self.view.convertRect(keyboardFrame, fromView:self.view.window)
            
            var contentViewFrame: CGRect = self.contentView.frame
            contentViewFrame.size.height = self.contentView.superview!.frame.size.height - keyboardFrame.size.height - contentViewFrame.origin.y
            
            self.boxView.autoresizingMask = [.FlexibleTopMargin, .FlexibleWidth]
            var boxViewFrame: CGRect = self.boxView.frame
            boxViewFrame.origin.y = contentViewFrame.size.height - boxViewFrame.size.height - 14
            
            var topViewFrame: CGRect = self.topView.frame
            topViewFrame.size.height = boxViewFrame.origin.y
            
            UIView.animateWithDuration(animationDuration.doubleValue, delay: 0, options: options, animations: {
                self.contentView.frame = contentViewFrame
                self.boxView.frame = boxViewFrame
                self.topView.frame = topViewFrame
            }, completion: { finished in
                
                
            })
        }
        
    }
    
    func keyboardWillHideNotification(aNotification: NSNotification) {
        if var info: [NSObject : AnyObject] = aNotification.userInfo {
            
            let animationDuration: NSNumber = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
            
            let animationCurve: NSNumber = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
            let options = UIViewAnimationOptions(rawValue: UInt(animationCurve.unsignedLongValue << 16))
            
            let contentViewFrame: CGRect = self.contentView.superview!.frame
            
            self.boxView.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleWidth]
            var boxViewFrame: CGRect = self.boxView.frame
            boxViewFrame.origin.y = (contentViewFrame.size.height - boxViewFrame.size.height) / 2
            
            var topViewFrame: CGRect = self.topView.frame
            topViewFrame.size.height = boxViewFrame.origin.y
            
            UIView.animateWithDuration(animationDuration.doubleValue, delay: 0, options: options, animations: {
                self.contentView.frame = contentViewFrame
                self.boxView.frame = boxViewFrame
                self.topView.frame = topViewFrame
                }, completion: { finished in
                    
            })
        }
    }
}
