//
//  ProgramsViewController.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/6/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

class ProgramsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView?
    
    var refreshControl: UIRefreshControl = UIRefreshControl()
    var programsArray: [[String: AnyObject]] = []
    var didUpdate = false

    
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.didUpdate {
            self.refreshControl.beginRefreshing()
            update()
            self.didUpdate = true
        }
    }

    // MARK: - Configure
    
    func configure() {
        configureNavigationBar()
        configureView()
        configureTableView()
    }
    
    func configureNavigationBar () {
        self.title = "Programas"
        
        let image: UIImage = UIImage.imageWithColor(UIColor.defaultNavigationBarColor(), size: CGSizeZero)
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.translucent = false
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func configureView() {
        self.view.backgroundColor = UIColor.defaultBackgroundColor()
    }
    
    func configureTableView() {
        self.tableView?.backgroundColor = self.view.backgroundColor;
        self.tableView?.separatorStyle = .None;
        
        
        self.refreshControl.tintColor = UIColor.defaultGrayColor()
        self.refreshControl.addTarget(self, action: #selector(self.update), forControlEvents: .ValueChanged)
        
        self.tableView?.addSubview(self.refreshControl)
        self.refreshControl.superview?.sendSubviewToBack(self.refreshControl)
    }
    
    //MARK: - Actions
    
    @IBAction func settings() {
        var title = ""
        
        if let currentUser = getCurrentUser() {
            title = currentUser[kCurrentUserNameKey] as! String
        }
        
        let logoutAlertController = UIAlertController.init(title: title, message: nil, preferredStyle: .ActionSheet)
        
        
        
        let logoutAlertAction = UIAlertAction.init(title: "Desconectar minha conta", style: .Destructive) { (alertAction) in
            setCurrentUser(nil)
            didLogout()
        }
        logoutAlertController.addAction(logoutAlertAction)
        
        let cancelAlertAction = UIAlertAction.init(title: "Cancelar", style: .Cancel, handler: nil)
        logoutAlertController.addAction(cancelAlertAction)
        
        self.presentViewController(logoutAlertController, animated: true, completion: nil)
    }
    
    // MARK: - Update
    
    func update() {
        myPrograms({ (programsArray) in
            self.refreshControl.endRefreshing()
            
            if let itemsArray = programsArray {
                self.programsArray = itemsArray
                self.tableView?.reloadData()
            }
            
            }, failure: { (error) in
                self.refreshControl.endRefreshing()
        })
    }

    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let width = self.view.frame.size.width - (2 * 14)
        return (width * 0.6) + 60
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let programDictionary: [String: AnyObject] = self.programsArray[indexPath.row]
        
        let cell: ProgramsTableViewCell = (tableView.dequeueReusableCellWithIdentifier("ProgramsTableViewCell") as? ProgramsTableViewCell)!
        cell.setProgram(programDictionary)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
