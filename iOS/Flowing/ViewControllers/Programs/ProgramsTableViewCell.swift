//
//  ProgramsTableViewCell.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/9/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import SDWebImage

class ProgramsTableViewCell: UITableViewCell {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var cardShadowView: UIView!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var programNameLabel: UILabel!
    @IBOutlet var programShortDescriptionLabel: UILabel!
    @IBOutlet var partnerPhotoImageView: UIImageView!
    @IBOutlet var partnerNameLabel: UILabel!
    @IBOutlet var partnerSignatureLabel: UILabel!
    @IBOutlet var rightArrowIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        resize()
    }
    
    // MARK - Configure
    
    func configure() {
        self.backgroundColor = UIColor.clearColor()
        
        configureCardView()
        configureCardShadowView()
    }
    
    func configureCardView() {
        self.cardView?.backgroundColor = UIColor.whiteColor()
        self.cardView?.layer.cornerRadius = 4
        self.cardView?.layer.borderWidth = 0
        self.cardView?.layer.borderColor = UIColor.defaultLineColor().CGColor
        self.cardView?.layer.masksToBounds = true
        
        configurePhotoImageView()
        configureProgramNameLabel()
        configureProgramShortDescriptionLabel()
        
        configurePartnerPhotoImageView()
        configurePartnerNameLabel()
        configurePartnerSignatureLabel()
    }
    
    func configureCardShadowView() {
        let scale: CGFloat = UIScreen.mainScreen().scale
        
        self.cardShadowView?.backgroundColor = self.cardView?.backgroundColor
        self.cardShadowView?.layer.cornerRadius = (self.cardView?.layer.cornerRadius)!
        self.cardShadowView?.layer.shadowColor = UIColor.blackColor().CGColor
        self.cardShadowView?.layer.shadowOffset = CGSizeMake(0, 1/scale)
        self.cardShadowView?.layer.shadowOpacity = 0.3
        self.cardShadowView?.layer.shadowRadius = 1/scale
    }
    
    func configurePhotoImageView() {
        self.photoImageView?.backgroundColor = UIColor.defaultLineColor()
    }
    
    func configureProgramNameLabel() {
        self.programNameLabel?.font = UIFont.customAvenirMediumFontWithSize(20)
        self.programNameLabel?.numberOfLines = 2
        self.programNameLabel?.backgroundColor = UIColor.randomColorWithAlpha(0)
    }
    
    func configureProgramShortDescriptionLabel() {
        self.programShortDescriptionLabel?.font = UIFont.customAvenirLightFontWithSize(16)
        self.programShortDescriptionLabel?.numberOfLines = 2
    }
    
    func configurePartnerPhotoImageView() {
        let scale: CGFloat = UIScreen.mainScreen().scale
        
        self.partnerPhotoImageView?.backgroundColor = UIColor.defaultBackgroundColor()
        self.partnerPhotoImageView?.layer.cornerRadius = (partnerPhotoImageView?.frame.size.height)! / 2
        self.partnerPhotoImageView?.layer.masksToBounds = true
        self.partnerPhotoImageView?.layer.borderWidth = 1/scale;
        self.partnerPhotoImageView?.layer.borderColor = UIColor.defaultLineColor().CGColor
    }
    
    func configurePartnerNameLabel() {
        self.partnerNameLabel?.font = UIFont.customAvenirNextMediumFontWithSize(16)
        self.partnerNameLabel?.textColor = UIColor.defaultTitleGrayTextColor()
    }
    
    func configurePartnerSignatureLabel() {
        self.partnerSignatureLabel?.font = UIFont.customAvenirNextRegularFontWithSize(14)
    }
    
    // MARK: - Resize
    
    func resize() {
        resizeProgramShortDescriptionLabel()
        resizeProgramNameLabel()
    }
    
    func resizeProgramShortDescriptionLabel() {
        var frame: CGRect = (self.programShortDescriptionLabel?.frame)!
        self.programShortDescriptionLabel?.sizeToFit()
        frame.size.height = (self.programShortDescriptionLabel?.frame.size.height)!
        frame.origin.y = (self.programShortDescriptionLabel?.superview?.frame.size.height)! - frame.size.height - 8
        self.programShortDescriptionLabel?.frame = frame
    }
    
    func resizeProgramNameLabel() {
        var frame: CGRect = (self.programNameLabel?.frame)!
        self.programNameLabel?.sizeToFit()
        frame.size.height = (self.programNameLabel?.frame.size.height)!
        frame.origin.y = (self.programShortDescriptionLabel?.frame.origin.y)! - frame.size.height
        self.programNameLabel?.frame = frame
    }
    
    // MARK: - setProgram
    
    func setProgram(programDictionary: [String: AnyObject]) {
        resetContent()
        
        if let programName: String = programDictionary["ProgramName"] as? String {
            programNameLabel?.text = programName;
        }
        
        if let programShortDescription: String = programDictionary["ProgramShortDesciption"] as? String {
            programShortDescriptionLabel?.text = programShortDescription;
        }
        
        if let partnerName: String = programDictionary["PartnerName"] as? String {
            partnerNameLabel?.text = partnerName;
        }
        
        if let partnerSignature: String = programDictionary["PartnerSignature"] as? String {
            partnerSignatureLabel?.text = partnerSignature;
        }
        
        if let photoUrl: String = programDictionary["ProgramRetinaImage_wide"] as? String {
            if let url: NSURL = NSURL.init(string: photoUrl) {
                self.photoImageView?.sd_setImageWithURL(url, placeholderImage: nil, completed: { (image, error, imageCacheType, url) in
                    if image != nil {
                        self.photoImageView?.image = image
                    }
                    else {
                        self.photoImageView?.image = nil
                    }
                });
            }
            
        }
        
        if let userPhotoUrl: String = programDictionary["PartnerProfilePicture"] as? String {
            if let url: NSURL = NSURL.init(string: userPhotoUrl) {
                self.partnerPhotoImageView?.sd_setImageWithURL(url, placeholderImage: nil, completed: { (image, error, imageCacheType, url) in
                    if image != nil {
                        self.partnerPhotoImageView?.image = image
                    }
                    else {
                        self.partnerPhotoImageView?.image = nil
                    }
                });
            }
            
        }
        
        resize()
    }
    
    func resetContent() {
        programNameLabel?.text = "";
        programShortDescriptionLabel?.text = "";
        partnerNameLabel?.text = "";
        partnerSignatureLabel?.text = "";
        self.photoImageView?.image = nil
        self.partnerPhotoImageView?.image = nil
    }
    
    // MARK: - Selection
    
    override func setSelected(selected: Bool, animated: Bool) {
        if selected {
            self.cardView?.backgroundColor = UIColor.defaultBlueColor()
            self.partnerNameLabel?.textColor = UIColor.whiteColor()
            self.partnerSignatureLabel?.textColor = UIColor.whiteColor()
            rightArrowIconImageView.maskImageViewWithColor(UIColor.whiteColor())
        }
        else {
            self.cardView?.backgroundColor = UIColor.whiteColor()
            self.partnerNameLabel?.textColor = UIColor.defaultTitleGrayTextColor()
            self.partnerSignatureLabel?.textColor = UIColor.defaultSubtitleGrayTextColor()
            rightArrowIconImageView.maskImageViewWithColor(UIColor.defaultTitleGrayTextColor())
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        self.setSelected(highlighted, animated: animated)
    }
}
