//
//  Helper.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/9/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit

let kCurrentUserKey: String = "currentUser"
let kCurrentUserNameKey: String = "FullName"
let kCurrentUserAccessTokenKey: String = "access_token"

class Helper: NSObject {
    
}

func setCurrentUser(currentUserDictionary: [String: AnyObject]?) {
    let userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    if let currentUser = currentUserDictionary {
        userDefaults.setValue(currentUser, forKey: kCurrentUserKey)
        userDefaults.setValue(currentUser["access_token"], forKey: kCurrentUserAccessTokenKey)
    }
    else {
        userDefaults.setValue(nil, forKey: kCurrentUserKey)
        userDefaults.setValue(nil, forKey: kCurrentUserAccessTokenKey)
    }
    userDefaults.synchronize()
}

func getCurrentUser() -> [String: AnyObject]? {
    let userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    if let currentUser = userDefaults.valueForKey(kCurrentUserKey) {
        return currentUser as? [String : AnyObject]
    }
    
    return nil
}

func getAccessToken() -> String? {
    if let currentUser: [String: AnyObject] = getCurrentUser() {
        if let accessToken = currentUser[kCurrentUserAccessTokenKey] {
            return accessToken as? String
        }
    }
    
    return nil
}

func didLogin() {
    let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    appDelegate.didLogin()
}

func didLogout() {
    let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    appDelegate.didLogout()
}