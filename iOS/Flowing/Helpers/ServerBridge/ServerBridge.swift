//
//  ServerBridge.swift
//  Flowing
//
//  Created by Lucas Algarra on 5/6/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import Alamofire

let kServerAddress: String = "http://dev.flowing.com.br"
let kServerversion: String = "v1"

enum LoginError {
    case LoginErrorGeneric
    case LoginErrorInvalidCredentials
}

class ServerBridge: NSObject {

}

// MARK: - Helpers

func headers() -> [String: String] {
    var headers: [String: String] = ["Content-Type": "application/x-www-form-encoded"]
    
    if let aceessToken = getAccessToken() {
        headers["Authorization"] = "bearer \(aceessToken)"
    }
    
    return headers
}

// MARK: - Login

func loginWithEmail(email: String, password: String, success: ((userDictionary: [String: AnyObject]?) -> Void)?, failure: ((LoginError) -> Void)?) {
    let url: String = "\(kServerAddress)/account/gettoken"
    
    let parameters = ["grant_type": "password", "username": email, "password": password]
    
    
    Alamofire.request(.POST, url, parameters: parameters).responseJSON { response in
        if response.response?.statusCode == 200 {
            if let resultDictionary: [String: AnyObject] = response.result.value as? [String : AnyObject] {
                setCurrentUser(resultDictionary)
                
                
                if let successBlock = success {
                    successBlock(userDictionary: resultDictionary)
                }
            }
            else if let failureBlock = failure {
                failureBlock(.LoginErrorGeneric)
            }
            
        }
        else if let failureBlock = failure {
            if response.response?.statusCode == 400 {
                failureBlock(.LoginErrorInvalidCredentials)
            }
            else {
                failureBlock(.LoginErrorGeneric)
            }
        }
    }
}

// MARK: - Programs

func myPrograms(success: ((programsArray: [[String: AnyObject]]?) -> Void)?, failure: ((NSError?) -> Void)?) {
    
    let url: String = "\(kServerAddress)/\(kServerversion)/users/recomendedprograms"
    
    Alamofire.request(.GET, url, parameters: nil, encoding: .JSON, headers: headers()).responseJSON { response in
        if let programsArray: [[String: AnyObject]] = response.result.value as? [[String : AnyObject]] {
            if let successBlock = success {
                successBlock(programsArray: programsArray)
            }
        }
        else if let failureBlock = failure {
            failureBlock(nil)
        }
    }
    
    
}

// MARK: - Coach

func myCoaches(success: ((coachesArray: [[String: AnyObject]]?) -> Void)?, failure: ((NSError?) -> Void)?) {
    
    if let currentUserAceessToken = getAccessToken() {
        let url: String = "\(kServerAddress)/\(kServerversion)/configurations/avatars"
        
        let headers = [
            "Authorization": "bearer \(currentUserAceessToken)",
            "Content-Type": "application/x-www-form-encoded"
        ]
        
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON, headers: headers).responseJSON { response in
            if let coachesArray: [[String: AnyObject]] = response.result.value as? [[String : AnyObject]] {
                if let successBlock = success {
                    successBlock(coachesArray: coachesArray)
                }
            }
            else if let failureBlock = failure {
                failureBlock(nil)
            }
        }
    }
    else if let failureBlock = failure {
        failureBlock(nil)
    }
    
    
}