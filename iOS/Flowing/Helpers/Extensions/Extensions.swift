//

//  Extensions.swift
//  Zazcar
//
//  Created by Lucas Algarra on 3/30/16.
//  Copyright © 2016 Lucas Algarra. All rights reserved.
//

import UIKit
import CoreGraphics

// MARK: - UIColor

extension UIColor {
    class func randomColor () -> UIColor {
        return randomColorWithAlpha(1)
    }
    
    class func randomColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:CGFloat(drand48()), green:CGFloat(drand48()) ,blue:CGFloat(drand48()) , alpha:alpha)
    }
    
    class func defaultBlueColor () -> UIColor {
        return defaultBlueColorWithAlpha(1)
    }
    
    class func defaultBlueColorWithAlpha(alpha: CGFloat) -> UIColor {
        //return UIColor(red:0/255.0, green:115/255.0 ,blue:139/255.0 , alpha:alpha)
        return UIColor(red:2/255.0, green:190/255.0 ,blue:216/255.0 , alpha:alpha)
    }
    
    class func defaultGrayColor () -> UIColor {
        return defaultGrayColorWithAlpha(1)
    }
    
    class func defaultGrayColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:70/255.0, green:76/255.0 ,blue:91/255.0 , alpha:alpha)
    }
    
    class func defaultGreenColor () -> UIColor {
        return defaultGreenColorWithAlpha(1)
    }
    
    class func defaultGreenColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:104/255.0, green:218/255.0 ,blue:102/255.0 , alpha:alpha)
    }
    
    // Background
    
    class func defaultBackgroundColor() -> UIColor {
        return defaultBackgroundColorWithAlpha(1)
    }
    
    class func defaultBackgroundColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:230/255.0, green:230/255.0 ,blue:230/255.0 , alpha:alpha)
    }
    
    // NavigationBar
    
    class func defaultNavigationBarColor() -> UIColor {
        return defaultGrayColor()
    }
    
    class func defaultNavigationBarColorWithAlpha(alpha: CGFloat) -> UIColor {
        return defaultBlueColorWithAlpha(alpha)
    }
    
    // Line
    
    class func defaultLineColor () -> UIColor {
        return defaultLineColorWithAlpha(1)
    }
    
    class func defaultLineColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:203/255.0, green:203/255.0 ,blue:203/255.0 , alpha:alpha)
    }
    
    // Text
    
    class func defaultTitleGrayTextColor () -> UIColor {
        return defaultTitleGrayTextColorWithAlpha(1)
    }
    
    class func defaultTitleGrayTextColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:116/255.0, green:116/255.0 ,blue:116/255.0 , alpha:alpha)
    }
    
    class func defaultSubtitleGrayTextColor () -> UIColor {
        return defaultSubtitleGrayTextColorWithAlpha(1)
    }
    
    class func defaultSubtitleGrayTextColorWithAlpha(alpha: CGFloat) -> UIColor {
        return UIColor(red:141/255.0, green:141/255.0 ,blue:141/255.0 , alpha:alpha)
    }
}

// MARK: - UIFont

extension UIFont {
    class func customAvenirMediumFontWithSize(size: CGFloat) -> UIFont? {
        return UIFont.init(name: "Avenir-Medium", size: size)
    }
    
    class func customAvenirLightFontWithSize(size: CGFloat) -> UIFont? {
        return UIFont.init(name: "Avenir-Light", size: size)
    }
    
    class func customAvenirNextRegularFontWithSize(size: CGFloat) -> UIFont? {
        return UIFont.init(name: "AvenirNext-Regular", size: size)
    }
    
    class func customAvenirNextMediumFontWithSize(size: CGFloat) -> UIFont? {
        return UIFont.init(name: "AvenirNext-Medium", size: size)
    }
}

// MARK: - UIImage

extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        var imageSize = size
        if (imageSize.width <= 0) {
            imageSize.width = 1
        }
        
        if (imageSize.height <= 0) {
            imageSize.height = 1
        }
        
        let rect: CGRect = CGRectMake(0.0, 0.0, imageSize.width, imageSize.height)
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
        
        color.setFill()
        UIRectFill(rect)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
}

//MARK: - UIImageView

extension UIImageView {
    func maskImageViewWithColor(color: UIColor){
        if let image: UIImage = self.image {
            self.image = image.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        }
        self.tintColor = color
    }
}